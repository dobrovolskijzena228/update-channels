/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.common.test;

import android.support.annotation.NonNull;
import junit.framework.Assert;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.fdroid.apputil.common.BuildConfig;
import org.fdroid.apputil.common.HURLDownloadStrategy;
import org.fdroid.apputil.common.OkHttp3DownloadStrategy;
import org.fdroid.apputil.common.StreamDownloadStrategy;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static org.junit.Assert.assertEquals;

public class StreamDownloadUnitTests {
    private CountDownLatch responseLatch;
    private Throwable testError;
    private int entryCount = 0;

    @Before
    public void setUp() {
        responseLatch = new CountDownLatch(1);
    }

    @Test
    public void hurlSuccess() throws Throwable {
        testSuccess(new HURLDownloadStrategy());
    }

    @Test
    public void hurlFailure() throws Throwable {
        testFailure(new HURLDownloadStrategy());
    }

    @Test
    public void okHttpSuccess() throws Throwable {
        final ProgressResponseBody.Listener progressListener =
                new ProgressResponseBody.Listener() {
                    long lastUpdateTime = 0L;

                    @Override
                    public void onProgressChange(long bytesRead,
                                                 long contentLength,
                                                 boolean done) {
                        long now = System.currentTimeMillis();

                        if (now - lastUpdateTime > 1000) {
                            System.out.println(String.format("FileDownloadUnitTests: downloaded %d of %d", bytesRead, contentLength));
                            lastUpdateTime = now;
                        }
                    }
                };

        Interceptor nightTrain = new Interceptor() {
            @Override
            public Response intercept(Chain chain)
                    throws IOException {
                Response original = chain.proceed(chain.request());
                Response.Builder b = original
                        .newBuilder()
                        .body(
                                new ProgressResponseBody(original.body(),
                                        progressListener));

                return (b.build());
            }
        };

        testSuccess(new OkHttp3DownloadStrategy(new OkHttpClient.Builder()
                .addNetworkInterceptor(nightTrain)));
    }

    @Test
    public void okHttpFailure() throws Throwable {
        testFailure(new OkHttp3DownloadStrategy());
    }

    private void testSuccess(StreamDownloadStrategy strategy) throws Throwable {
        strategy.download(BuildConfig.TEST_URL_STREAM,
                new StreamDownloadStrategy.Callback() {
                    @Override
                    public void onSuccess(@NonNull String url,
                                          @NonNull InputStream content) throws IOException {
                        ZipInputStream zis = new ZipInputStream(content);

                        for (ZipEntry entry; (entry = zis.getNextEntry()) != null; ) {
                            entryCount++;
                            System.out.println("FileDownloadUnitTests: " + entry.getName());
                        }

                        zis.close();

                        responseLatch.countDown();
                    }

                    @Override
                    public void onError(String url, Throwable t) {
                        testError = t;
                        responseLatch.countDown();
                    }
                });

        responseLatch.await(10, TimeUnit.SECONDS);

        if (testError != null) {
            throw testError;
        }

        assertEquals(4, entryCount);
    }

    private void testFailure(StreamDownloadStrategy strategy) throws Throwable {
        strategy.download(BuildConfig.TEST_URL_FAILURE,
                new StreamDownloadStrategy.Callback() {
                    @Override
                    public void onSuccess(@NonNull String url,
                                          @NonNull InputStream content) {
                        responseLatch.countDown();
                    }

                    @Override
                    public void onError(String url, Throwable t) {
                        testError = t;
                        responseLatch.countDown();
                    }
                });

        responseLatch.await(60, TimeUnit.SECONDS);

        Assert.assertNotNull(testError);
    }
}
