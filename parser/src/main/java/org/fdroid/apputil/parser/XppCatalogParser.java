/*
 * Copyright (c) 2017 CommonsWare, LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.fdroid.apputil.parser;

import android.support.annotation.NonNull;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Parser of XML-formatted catalogs, using Android's XmlPullParser
 */
public class XppCatalogParser extends CatalogParser {
    private static final SimpleDateFormat YYYYMMDD =
            new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private static final String TAG_CATALOG = "fdroid";
    private static final String TAG_REPO = "repo";
    private static final String TAG_INSTALL = "install";
    private static final String TAG_APPLICATION = "application";
    private static final String ATTR_PACKAGE_NAME = "packageName";
    private static final String ATTR_ICON = "icon";
    private static final String ATTR_MAXAGE = "maxage";
    private static final String ATTR_NAME = "name";
    private static final String ATTR_PUBKEY = "pubkey";
    private static final String ATTR_TIMESTAMP = "timestamp";
    private static final String ATTR_URL = "url";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_MIRROR = "mirror";
    private static final String ATTR_VERSION = "version";
    private static final String ATTR_ID = "id";
    private static final String TAG_ADDED = "added";
    private static final String TAG_LAST_UPDATED = "lastupdated";
    private static final String TAG_NAME = "name";
    private static final String TAG_SUMMARY = "summary";
    private static final String TAG_ICON = "icon";
    private static final String TAG_DESC = "desc";
    private static final String TAG_LICENSE = "license";
    private static final String TAG_CATEGORIES = "categories";
    private static final String TAG_CATEGORY = "category";
    private static final String TAG_WEB = "web";
    private static final String TAG_SOURCE = "source";
    private static final String TAG_TRACKER = "tracker";
    private static final String TAG_BITCOIN = "bitcoin";
    private static final String TAG_MARKETVERSION = "marketversion";
    private static final String TAG_MARKETVERCODE = "marketvercode";
    private static final String TAG_PACKAGE = "package";
    private static final String TAG_VERSION = "version";
    private static final String TAG_VERSIONCODE = "versioncode";
    private static final String TAG_APKNAME = "apkname";
    private static final String TAG_SIZE = "size";
    private static final String TAG_SDKVER = "sdkver";
    private static final String TAG_TARGET_SDK_VERSION = "targetSdkVersion";
    private static final String TAG_SIG = "sig";
    private static final String TAG_PERMISSIONS = "permissions";
    private static final String TAG_NATIVECODE = "nativecode";
    private static final String ATTR_TYPE = "type";
    private static final String TAG_HASH = "hash";
    private final XmlPullParserFactory factory;

    /**
     * Standard constructor
     *
     * @throws XmlPullParserException because... reasons?
     */
    public XppCatalogParser() throws XmlPullParserException {
        factory = XmlPullParserFactory.newInstance();
    }

    /**
     * @{inheritDoc}
     */
    @Override
    public Catalog parse(@NonNull InputStream in, ApplicationFilter filter)
            throws Exception {
        ZipInputStream zis = new ZipInputStream(in);
        Catalog catalog = null;

        for (ZipEntry entry; (entry = zis.getNextEntry()) != null; ) {
            if ("index.xml".equals(entry.getName())) {
                catalog = parseXml(new InputStreamReader(zis), filter);
                break;
            }
        }

        zis.close();

        return (catalog);
    }

    private Catalog parseXml(Reader in, ApplicationFilter filter) throws Exception {
        XmlPullParser xpp = factory.newPullParser();
        Catalog catalog = null;

        xpp.setInput(in);

        int eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG &&
                    TAG_CATALOG.equals(xpp.getName())) {
                catalog = parseCatalog(xpp, filter);
            }

            eventType = xpp.next();
        }

        return (catalog);
    }

    private Catalog parseCatalog(XmlPullParser xpp, ApplicationFilter filter)
            throws IOException, XmlPullParserException, ParseException {
        Catalog result = new Catalog();
        int eventType = xpp.next();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (TAG_REPO.equals(xpp.getName())) {
                    result.repo(parseRepo(xpp));
                } else if (TAG_INSTALL.equals(xpp.getName())) {
                    result.install(parseInstall(xpp));
                } else if (TAG_APPLICATION.equals(xpp.getName())) {
                    Application app = parseApp(xpp);

                    if (filter == null || filter.isAcceptable(app)) {
                        result.addApp(app);
                    }
                }
            } else if (eventType == XmlPullParser.END_TAG &&
                    TAG_CATALOG.equals(xpp.getName())) {
                break;
            }

            eventType = xpp.next();
        }

        return (result);
    }

    private Repo parseRepo(XmlPullParser xpp)
            throws IOException, XmlPullParserException {
        Repo repo = new Repo();

        repo.icon(xpp.getAttributeValue(null, ATTR_ICON));
        repo.maxage(intifyAttr(xpp, ATTR_MAXAGE));
        repo.name(xpp.getAttributeValue(null, ATTR_NAME));
        repo.pubkey(xpp.getAttributeValue(null, ATTR_PUBKEY));
        repo.timestamp(Long.parseLong(xpp.getAttributeValue(null, ATTR_TIMESTAMP)));
        repo.url(xpp.getAttributeValue(null, ATTR_URL));
        repo.version(intifyAttr(xpp, ATTR_VERSION));

        int eventType = xpp.next();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (TAG_DESCRIPTION.equals(xpp.getName())) {
                    repo.description(stringify(xpp, TAG_DESCRIPTION));
                } else if (TAG_MIRROR.equals(xpp.getName())) {
                    repo.addMirror(stringify(xpp, TAG_MIRROR));
                }
            } else if (eventType == XmlPullParser.END_TAG &&
                    TAG_REPO.equals(xpp.getName())) {
                break;
            }

            eventType = xpp.next();
        }

        return (repo);
    }

    private Install parseInstall(XmlPullParser xpp) {
        Install install = new Install();

        install.packageName(xpp.getAttributeValue(null, ATTR_PACKAGE_NAME));

        return (install);
    }

    private Application parseApp(XmlPullParser xpp)
            throws IOException, XmlPullParserException, ParseException {
        Application app = new Application();

        app.id(xpp.getAttributeValue(null, ATTR_ID));

        int eventType = xpp.next();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (TAG_ADDED.equals(xpp.getName())) {
                    app.added(dateify(stringify(xpp, TAG_ADDED)));
                } else if (TAG_LAST_UPDATED.equals(xpp.getName())) {
                    app.lastUpdated(dateify(stringify(xpp, TAG_LAST_UPDATED)));
                } else if (TAG_NAME.equals(xpp.getName())) {
                    app.name(stringify(xpp, TAG_NAME));
                } else if (TAG_SUMMARY.equals(xpp.getName())) {
                    app.summary(stringify(xpp, TAG_SUMMARY));
                } else if (TAG_ICON.equals(xpp.getName())) {
                    app.icon(stringify(xpp, TAG_ICON));
                } else if (TAG_DESC.equals(xpp.getName())) {
                    app.desc(stringify(xpp, TAG_DESC));
                } else if (TAG_LICENSE.equals(xpp.getName())) {
                    app.license(stringify(xpp, TAG_LICENSE));
                } else if (TAG_CATEGORIES.equals(xpp.getName())) {
                    app.addCategories(stringify(xpp, TAG_CATEGORIES).split(","));
                } else if (TAG_CATEGORY.equals(xpp.getName())) {
                    app.category(stringify(xpp, TAG_CATEGORY));
                } else if (TAG_WEB.equals(xpp.getName())) {
                    app.web(stringify(xpp, TAG_WEB));
                } else if (TAG_SOURCE.equals(xpp.getName())) {
                    app.source(stringify(xpp, TAG_SOURCE));
                } else if (TAG_TRACKER.equals(xpp.getName())) {
                    app.tracker(stringify(xpp, TAG_TRACKER));
                } else if (TAG_BITCOIN.equals(xpp.getName())) {
                    app.bitcoin(stringify(xpp, TAG_BITCOIN));
                } else if (TAG_MARKETVERSION.equals(xpp.getName())) {
                    app.marketversion(stringify(xpp, TAG_MARKETVERSION));
                } else if (TAG_MARKETVERCODE.equals(xpp.getName())) {
                    app.marketvercode(stringify(xpp, TAG_MARKETVERCODE));
                } else if (TAG_PACKAGE.equals(xpp.getName())) {
                    app.addPackage(parsePackage(xpp));
                }
            } else if (eventType == XmlPullParser.END_TAG &&
                    TAG_APPLICATION.equals(xpp.getName())) {
                break;
            }

            eventType = xpp.next();
        }

        return (app);
    }

    private PackageSpec parsePackage(XmlPullParser xpp)
            throws IOException, XmlPullParserException, ParseException {
        PackageSpec pkg = new PackageSpec();

        int eventType = xpp.next();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.START_TAG) {
                if (TAG_VERSION.equals(xpp.getName())) {
                    pkg.version(stringify(xpp, TAG_VERSION));
                } else if (TAG_VERSIONCODE.equals(xpp.getName())) {
                    pkg.versioncode(intifyElement(xpp, TAG_VERSIONCODE));
                } else if (TAG_APKNAME.equals(xpp.getName())) {
                    pkg.apkname(stringify(xpp, TAG_APKNAME));
                } else if (TAG_HASH.equals(xpp.getName())) {
                    pkg.hashType(xpp.getAttributeValue(null, ATTR_TYPE));
                    pkg.hashValue(stringify(xpp, TAG_HASH));
                } else if (TAG_SIZE.equals(xpp.getName())) {
                    pkg.size(intifyElement(xpp, TAG_SIZE));
                } else if (TAG_SDKVER.equals(xpp.getName())) {
                    pkg.sdkver(stringify(xpp, TAG_SDKVER));
                } else if (TAG_TARGET_SDK_VERSION.equals(xpp.getName())) {
                    pkg.targetSdkVersion(stringify(xpp, TAG_TARGET_SDK_VERSION));
                } else if (TAG_ADDED.equals(xpp.getName())) {
                    pkg.added(dateify(stringify(xpp, TAG_ADDED)));
                } else if (TAG_SIG.equals(xpp.getName())) {
                    pkg.sig(stringify(xpp, TAG_SIG));
                } else if (TAG_PERMISSIONS.equals(xpp.getName())) {
                    pkg.addPermissions(stringify(xpp, TAG_PERMISSIONS).split(","));
                } else if (TAG_NATIVECODE.equals(xpp.getName())) {
                    pkg.addNativecode(stringify(xpp, TAG_NATIVECODE).split(","));
                }
            } else if (eventType == XmlPullParser.END_TAG &&
                    TAG_PACKAGE.equals(xpp.getName())) {
                break;
            }

            eventType = xpp.next();
        }

        return (pkg);
    }

    private int intifyAttr(XmlPullParser xpp, String attr) {
        String value = xpp.getAttributeValue(null, attr);

        if (value != null) {
            try {
                return (Integer.parseInt(value));
            } catch (NumberFormatException e) {
                // invalid, ignoring
            }
        }

        return (0);
    }

    private int intifyElement(XmlPullParser xpp, String tag)
            throws IOException, XmlPullParserException {
        String value = stringify(xpp, tag);

        if (value != null) {
            try {
                return (Integer.parseInt(value));
            } catch (NumberFormatException e) {
                // invalid, ignoring
            }
        }

        return (0);
    }

    private String stringify(XmlPullParser xpp, String tag)
            throws IOException, XmlPullParserException {
        StringBuilder buf = new StringBuilder();

        int eventType = xpp.next();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            if (eventType == XmlPullParser.END_TAG) {
                if (tag.equals(xpp.getName())) {
                    break;
                } else {
                    buf.append("</");
                    buf.append(xpp.getName());
                    buf.append(">");
                }
            } else if (eventType == XmlPullParser.START_TAG) {
                buf.append("<");
                buf.append(xpp.getName());
                buf.append(">");
            } else if (eventType == XmlPullParser.TEXT) {
                buf.append(xpp.getText());
            }

            eventType = xpp.next();
        }

        return (buf.toString().trim());
    }

    private long dateify(String yyyymmdd) throws ParseException {
        Date date = YYYYMMDD.parse(yyyymmdd);

        return (date.getTime());
    }
}
